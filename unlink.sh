#! /bin/bash

set -e

DOTFILES=$(dirname $0)
SKIP_FILES='skip_files'
SKIP_DIRS='skip_dirs'

cd $DOTFILES

echo "> unlinking regular files"
for i in $(find . -maxdepth 1 -type f -name ".*" | sort); do
    base=$(basename $i)
    if [[ -n "$(\egrep -o $base$ $SKIP_FILES)" ]]; then
        echo "skipping $base"
        continue
    fi
    printf "checking $base... "
    t="$HOME/$base"
    if [[ -h "$t" ]]; then
        echo "$t found, removing link"
        rm $t
        continue
    fi
    if [[ -f "$t" ]]; then
        echo "$t is a regular file, skipping"
        continue
    fi
    echo "not found"
done

echo "> unlinking directories"
for i in $(find . -maxdepth 1 -type d -regex './\..*'| sort); do
    base=$(basename $i)
    if [[ -n "$(\grep -o $base$ $SKIP_DIRS)" ]]; then
        echo "skipping $base"
        continue
    fi
    printf "checking dir $base... "
    t="$HOME/$base"
    if [[ -h "$t" ]]; then
        echo "$t found, removing link"
	rm $t
        continue
    fi
    if [[ -d "$t" ]]; then
        echo "$t is a regular directory, skipping"
        continue
    fi
    echo "not found"
done
