################################################################################
#                                                                              #
#                    SysAdmin eXperience (SAX) enhancements                    #
#                             A ZSH user perspective                           #
#                                                                              #
################################################################################
#
# Table of contents
#   0. About this document
#   0.1. License
#   0.2. Preface
#   0.3. Planned Features
#   1. Biases
#   2. ZSH and Defaults
#   3. Applications
#   4. Resources
#   4.1. Xorg
#   4.1.1. libinput / xinput
#   4.1.2. Remap Caps-Lock with Esc
#   4.1.3. Custom configuration for X values
#
################################################################################
#                                                                              #
# 0. About this document ----------------------------------------------------- #
#                                                                              #
# 0.1. License
#
# TODO: Add GNU GPL 3.0 for ZSH script and some GNU GPL related license for
# TODO: documentation.
#
# 0.2. Preface
#
# This resource file for ZSH shell is intended to be read as a document and be
# self-contained with all necessary commands to prepare a new ZSH shell
# environment.
#
# All of this started as my personal .zshrc file to be self-contained and
# self-explained to save me time looking up for references in my bookmarks or
# elsewhere and remind my of several reasons behind weird settings. I'm planning
# to do so as well in my .vimrc, .tmux.conf and Alacritty.yml config files.
#
# If you find this document/script useful please reuse it, fork it, contribute
# to it. Nevertheless I'll end up deciding what to put here, you're free to use
# your own fork. Maybe `git add --patch` contributions would be more fit for
# one-file changes.
#
# 0.3. Planned Features
#
# - [ ] VIM markers for foldable sections.
# - [ ] Self-contained function to transform the very same document to Markdown (or
#   any other well-established plain-text format).
#
# 1. Biases --------------------------------------------------------------------
#
# I'm a hard-die fan of Vim, Tmux and colorful (light) terminal emulator styling,
# so you'll find some inclinations to support more those tools and have here and
# there color settings with light blue, light yellow (as solarized theme) and what
# not.
#
# I almost always use Alacritty as my default terminal emulator since it's very
# stable and I started to put more trust in rust-built software in leau de
# c(pp)-built software.
#
# 2. ZSH and Defaults ----------------------------------------------------------
#

# The following lines were added by compinstall
zstyle :compinstall filename '/home/joshua/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Enable (and completely trust in) direnv with eval
if whence direnv >/dev/null; then; eval "$(direnv hook zsh)"; fi

# General env vars
export OS=$(cat /etc/os-release | awk -F"=" '/^ID=/ { print $2 }')
export TMP="$HOME/downloads/tmp"
export KUBECONFIG="$HOME/.kube/config" #..............Add KUBECONFIG file to env
export VIRTDOCS="$HOME/virtdocs" #..................... Add VIRTDOCS path to env
export VIRTLIB="$HOME/virtlib" #........................ Add VIRTLIB path to env
export VIRTNOTES="$HOME/virtnotes" #.................. Add VIRTNOTES path to env
export VIRTWK="$HOME/virtwk" #........................... Add VIRTWK path to env

# AWS env vars
export AWS_N_VIRGINIA="us-east-1"
export AWS_OHIO="us-east-2"
export AWS_N_CALIFORNIA="us-west-1"
export AWS_OREGON="us-west-2"

PROC_MANAGER=""
which service &>/dev/null; IS_DEBIAN_SERVICE=$?
if [ -z $PROC_MANAGER -a $IS_DEBIAN_SERVICE -eq 0 ]; then; PROC_MANAGER="debian-service"; fi
which systemctl &>/dev/null; IS_SYSTEMD=$?
if [ -z $PROC_MANAGER -a $IS_SYSTEMD -eq 0 ]; then; PROC_MANAGER="systemd"; fi
export PROC_MANAGER

ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="josh-sorin"
JIRA_URL="https://airtech.atlassian.net"

plugins=(vi-mode git jira zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# Personal preferences
export EDITOR=vim
export PAGER=less
#export LESS= # LESS configuration was moved to .lesskey config file

export HISTSIZE=10000000
export SAVEHIST=10000000

setopt appendhistory autocd
unsetopt beep
export HISTFILE=~/.zsh_history

zsh_general_aliases="$HOME/.zsh-general-aliases"
zsh_places_aliases="$HOME/.zsh-places-aliases"
zsh_other_aliases="$HOME/.zsh-other-aliases"
zsh_utils="$HOME/.zsh-utils"

if [ -f "$zsh_general_aliases" ]; then source $zsh_general_aliases; fi
if [ -f "$zsh_places_aliases" ]; then source $zsh_places_aliases; fi
if [ -f "$zsh_other_aliases" ]; then source $zsh_other_aliases; fi
if [ -f "$zsh_utils" ]; then source $zsh_utils; fi
#
# 3. Applications --------------------------------------------------------------

# Necessary env vars needed by these programs (exact name)
export DENO_DIR="$HOME/.deno"
export GOPATH="$HOME/wk/go"
export JAVA_HOME="/usr/lib/jvm/default-java"

# Convenient env vars to locate manually installed programs
export ANDROID_DIR="$HOME/wk/local/android/sdk2"
export BABASHKA_DIR="$HOME/.babashka"
export BBIN_DIR="$BABASHKA_DIR/bbin"
export CARGO_DIR="$HOME/.cargo"
export CCLOUD_DIR="$HOME/wk/local/ccloud"
export FLUTTER_DIR="$HOME/downloads/packages/flutter-from-android-studio/flutter"
export FLYWAY_DIR="$HOME/wk/local/flyway"
export GOLANG_DIR="/usr/local/go"
export INTELLIJ_DIR="/opt/intellij"
export KAFKA_DIR="$HOME/wk/local/kafka"
export LOCAL_DIR="$HOME/.local"
export NVM_DIR="$HOME/.nvm"
export OKTA_DIR="$HOME/.okta"
export OPENSTUDIO_HPXML_DIR="$HOME/.local/share/OpenStudio-HPXML"
export RVM_DIR="$HOME/.rvm"
export SDKMAN_DIR="$HOME/.sdkman"

# Expand PATH to include installed program's executable files
export PATH="$PATH:$BBIN_DIR/bin"
export PATH="$PATH:$CARGO_DIR/bin"
export PATH="$PATH:$CCLOUD_DIR/bin"
export PATH="$PATH:$DENO_DIR/bin"
export PATH="$PATH:$FLUTTER_DIR/bin"
export PATH="$PATH:$FLYWAY_DIR"
export PATH="$PATH:$GOLANG_DIR/bin"
export PATH="$PATH:$GOPATH/bin"
export PATH="$PATH:$INTELLIJ_DIR/bin"
export PATH="$PATH:$KAFKA_DIR/bin"
export PATH="$PATH:$LOCAL_DIR/bin"
export PATH="$PATH:$OKTA_DIR/bin"
export PATH="$PATH:$RVM_DIR/bin"


# SSH Agent process watcher by `keychain` program
# For historic reference, check manual setup
# Ref: https://askubuntu.com/questions/36255/why-wont-ssh-agent-save-my-unencrypted-key-for-later-use
if which keychain >/dev/null; then
    eval $(keychain --eval --quiet -Q --systemd --timeout 240)
fi
# --eval    Output script to calling shell
# --quiet   Do not output greeting message
# -Q        Bypass key verification if agent exists with one or more keys added
# --timeout Set agent with timeout of n minutes

# Default behavour is to initialize always a tmux session if shell is running on
# bare emulator. If shell runs within tmux session, ignore bootstrapping.
# Randomly assign a 3 chars id to newly created sessions. Use tmuxinator to easily
# load a predefined session by its name or just mirror an existing session.
if [[ $TERM != "screen-256color" ]]; then
    TID=$(echo $RANDOM | sha1sum | head -c 3)
    export TERM="xterm-256color"
    tmux attach-session -t "$TID" || tmux new-session -s "$TID"
    exit
fi
#
# 4. Resources -----------------------------------------------------------------
#
# 4.1. Xorg
#
# When it comes to bare bones configuration of Xorg for mouse, keys mapping,
# initialization, etc, you end up looking up and looking up again in a search
# engine. Here I summarize my findings pertaining to my Samsung NP900 when I
# needed something in particular.
#
# 4.1.1. libinput / xinput
#
# If you want to configure your touchpad at the lowest level (afaik) you use
# libinput and xinit packages. For instance you can get a list of input devices
# with
# > libinput-list-devices
# or
# > xinput list
# and with the latter's result and the device's id of interest you can inspect
# its capabilities with
# > xinput list-props <device-id>
# Afterwards, you can change values with set-prop
# > xinput set-prop <device-id> <prop-id> <values>
#
# For more information please refer to this wiki
# https://wiki.archlinux.org/index.php/Libinput
#
# 4.1.2. Remap Caps-Lock with Esc
#
# If you're a Vim power user and your DE (if any) does not provide you with
# sufficient customization, you can always resort to Xorg utils to remap keys
# mappings. I read in barely recent (2015, today is 20170627) forums that
# xmodmap is deprecated so we will use setxkbmap
# > setxkbmap -option caps:escape
#
# For more information please refer to this forum thread
# https://askubuntu.com/a/614664
#
# 4.1.3. Custom configuration for X values
#
# Gnome 3 does not allow to set values for Mouse velocity or Keyboard repetition
# beyond of those limited by the UI. I set manually those values with `xset`

# Set autorepeat for all keys with delay of 300ms and key press repetition at roughly 80 hits per second
xset r rate 300 80

# For mor information, please refer to this SuperUser thread
# https://superuser.com/questions/158127/how-to-increase-mouse-cursor-speed-in-ubuntu-gnome/158164#158164
# and actual man page for xset command.
#
################################################################################

# Cargo
[[ -s "$CARGO_DIR/env" ]] && source "$CARGO_DIR/env"

# SDKMAN
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"

# NVM
[[ -s "$NVM_DIR/nvm.sh" ]] && source "$NVM_DIR/nvm.sh"
[[ -s "$NVM_DIR/bash_completion" ]] && source "$NVM_DIR/bash_completion"

# RVM
[[ -s "$RVM_DIR/scripts/rvm" ]] && source "$RVM_DIR/scripts/rvm"
