" general
execute pathogen#infect()
syntax on                    " Highlight syntax
filetype plugin indent on    " ???

" settings
set enc=utf-8                  " default encoding
set fillchars+=vert:\│         " vert split with proper filling chars
set nocompatible               " vim superpowers
set noautochdir                " don't switch CWD implicitly
set path+=**                   " recursive finding
set path-=/usr/include         " don't pollute finding with c headers
set wildmenu                   " show alternatives in command buffer
set backup                     " make backups
set directory=~/.vim/tmp       " directory for swap files
set backupdir=~/.vim/backup    " ......... ... backup .....
set fileformats=unix,dos,mac   " support all three, in this order
set hidden                     " enable buffer navigation without saving
set scrolloff=1                " keep 1 line of separation near top and bottom
set showcmd                    " display command being typed
set showmatch                  " show matching brackets
set incsearch                  " incrementally highlight as keys are typed
set hlsearch                   " highlight search results
set nowrap                     " overflow lines beyond screen
set ruler                      " always show line and column
set laststatus=2               " always show status line
set splitbelow                 " always put new window below when spliting
set splitright                 " always put new window to the left when vspliting
set backspace=indent,eol,start " allow backspace to delete from auto-indent, eol or start from insertion
set colorcolumn=0              " disable color column delimiter
set clipboard^=unnamedplus     " unleash seamless clipboard interop! [s1]
" [s1] https://stackoverflow.com/questions/23946289/vim-change-default-paste-register#23947324
set ignorecase                 " automatically match case insensitive patterns [s2]
set smartcase                  " whenever i use upper case, the matching is converted to case sensitive [s2]
" [s2] https://stackoverflow.com/questions/2287440/how-to-do-case-insensitive-search-in-vim
set mouse=a                    " enable mouse for moving and clicking through windows

" when mouse is enabled and Vim is running inside tmux, let it work properly
" https://superuser.com/questions/549930/cant-resize-vim-splits-inside-tmux/550482#550482
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif


let g:LookOfDisapprovalTabTreshold=0
let g:LookOfDisapprovalSpaceTreshold=0

" settings based on file name pattern
" ([no]et -> [no]expandtab: [not] expand tabs into spaces)
" (ts -> tabstop: n spaces displayed for a <Tab>)
" (sts -> softtabstop: n spaces when <Tab> is hit)
" (sw -> shiftwidth: auto indentation is of n spaces)
" (ft -> filetype)
au BufRead,BufNewFile *.cr,*.rb,*.yml,*.js,*.mjs,*.json,*.vue,*.sh set et ts=2 sts=2 sw=2
au BufRead,BufNewFile *.go,*.coffee,*.scala                  set noet ts=4 sts=4 sw=4
au BufRead,BufNewFile nginx.conf                             set et ts=4 sts=4 sw=4
au BufRead,BufNewFile Dockerfile.*                           set ft=dockerfile
au BufRead,BufNewFile *.sql                                  set ft=pgsql
au BufRead,BufNewFile application.conf                       set ft=sbt.config
au BufRead,BufNewFile .eslintrc,.sequelizerc                 set ft=javascript

" mappings
let mapleader = " "

" force convenient shortcuts or make it more conscious
nnoremap <Leader><Leader>r :source $MYVIMRC<CR>
nnoremap <Leader><Leader>R :redraw!<CR>
nnoremap <Leader><Leader>l :set list!<CR>
nnoremap <Leader><Leader>n :nohlsearch<CR>
nnoremap <Leader><Leader>p :set paste!<CR>
nnoremap <Leader><Leader>q :qa<CR>
nnoremap <Leader><Leader>b :%bd<CR>
nnoremap <Leader><Leader>w :wa<CR>

" potentially destructive and loss of changes
nnoremap <Leader><Leader><Leader>q :qa!<CR>
nnoremap <Leader><Leader><Leader>w :wa!<CR>

" work with buffers
nnoremap <M-l> <ESC>:bnext<CR>
nnoremap <M-h> <ESC>:bprevious<CR>

nnoremap <Leader>w :w<CR>
nnoremap <Leader>d :bd<CR>

nnoremap <Leader><M-k> <ESC>:buffer 
nnoremap <Leader><M-j> <ESC>:buffers<CR>

" move around windows
nnoremap <C-j> <C-W><C-J>
nnoremap <C-k> <C-W><C-K>
nnoremap <C-l> <C-W><C-L>
nnoremap <C-h> <C-W><C-H>

nnoremap <Leader>e :Explore<CR>
nnoremap <Leader>q :q<CR>

nnoremap <Leader>v <C-W><C-V>
nnoremap <Leader>s <C-W><C-S>
nnoremap <Leader>= <C-W>=

" work with tabs (layouts)
nnoremap <M-j> <ESC>:tabprevious<CR>
nnoremap <M-k> <ESC>:tabnext<CR>

nnoremap <Leader><M-v> <ESC>:tabnew<CR>
nnoremap <Leader><M-d> <ESC>:tabclose<CR>

" in normal mode, issue commands
nnoremap <C-p> "+p
nnoremap <C-S-p> "+P

" in input mode, issue commands
"inoremap jj <Esc> " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap kk <Esc> " This mapping conflicted when pasting encoded data, such as PEM files

inoremap <C-h> <C-o>:TmuxNavigateLeft<cr>
inoremap <C-j> <C-o>:TmuxNavigateDown<cr>
inoremap <C-k> <C-o>:TmuxNavigateUp<cr>
inoremap <C-l> <C-o>:TmuxNavigateRight<cr>
inoremap <C-\> <C-o>:TmuxNavigatePrevious<cr>

" quick movements - https://vim.fandom.com/wiki/Quick_command_in_insert_mode
"inoremap II <Esc>I " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap AA <Esc>A " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap OO <Esc>O " This mapping conflicted when pasting encoded data, such as PEM files

" line modifications - https://vim.fandom.com/wiki/Quick_command_in_insert_mode
"inoremap CC <Esc>C " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap SS <Esc>S " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap DD <Esc>dd " This mapping conflicted when pasting encoded data, such as PEM files
"inoremap UU <Esc>u " This mapping conflicted when pasting encoded data, such as PEM files

" in visual mode, issue commands
vnoremap <C-e> <Nop>
vnoremap <C-y> "+y

" netrw configuration
let g:netrw_banner=0           " do not show initial banner
let g:netrw_liststyle=0        " one line per file, tree like listing is broken with given list_hide
let g:netrw_winsize=80         " percentage of netrw window size taken for new window
let g:netrw_list_hide= '^\..*' " Only hide files that start with dot (.)
let g:netrw_bufsettings = 'nomodifiable nomodified readonly nobuflisted wrap number' " Same as noma nomod ro nobl nowrap nu


" disapprove-deep-indentation configuration
au BufRead,BufNewFile *.js let g:LookOfDisapprovalTabTreshold=0 "2
au BufRead,BufNewFile *.js let g:LookOfDisapprovalSpaceTreshold=0 "10

" [DISABLED] ctrlp configuration
"set wildignore+=*/node_modules/*
"set wildignore+=*/coverage/*

" general commands
command! Ealacritty edit $HOME/.config/alacritty/alacritty.yml
command! Etmuxconf edit $HOME/.tmux.conf
command! Evimrc edit $MYVIMRC
command! Egvimrc edit $MYGVIMRC
command! Ezshrc edit $HOME/.zshrc
command! QQ :q!

" airline and pencil specific
let g:pencil_higher_contrast_ui = 0 " previous value set to 1
let g:airline_theme='pencil'
let g:airline_powerline_fonts = 1
set noshowmode

" diminactive specific
let g:diminactive_use_colorcolumn = 1
let g:diminactive_use_syntax = 1

" rainbow activation
let g:rainbow_active = 1
let g:rainbow_conf = {
\    'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
\    'ctermfgs': ['blue', 'cyan', 'magenta', 'brown']
\}

" color scheme specific
exec ':colorscheme default'
"" light context
set background=light
hi ColorColumn term=reverse ctermbg=251 guibg=Gray
hi VertSplit   term=reverse cterm=NONE ctermfg=255 ctermbg=0
"" dark context
"set background=dark
"hi ColorColumn term=reverse ctermbg=238 guibg=DarkGray
"hi VertSplit   term=reverse cterm=NONE ctermfg=255 ctermbg=0
" exec ':colorscheme xoria256'
" exec ':colorscheme wombat256mod'
" exec ':colorscheme PaperColor'
" exec ':colorscheme pencil'

" Hacks and Workarounds...

" [Avoid scrolling when switch buffers]
" http://vim.wikia.com/wiki/Avoid_scrolling_when_switch_buffers
"
" When leaving and entering buffers, we hope to retain relative position of
" the cursor in the current window.

" Save current view settings on a per-window, per-buffer basis.
function! AutoSaveWinView()
    if !exists("w:SavedBufView")
        let w:SavedBufView = {}
    endif
    let w:SavedBufView[bufnr("%")] = winsaveview()
endfunction

" Restore current view settings.
function! AutoRestoreWinView()
    let buf = bufnr("%")
    if exists("w:SavedBufView") && has_key(w:SavedBufView, buf)
        let v = winsaveview()
        let atStartOfFile = v.lnum == 1 && v.col == 0
        if atStartOfFile && !&diff
            call winrestview(w:SavedBufView[buf])
        endif
        unlet w:SavedBufView[buf]
    endif
endfunction

" When switching buffers, preserve window view.
if v:version >= 700
    autocmd BufLeave * call AutoSaveWinView()
    autocmd BufEnter * call AutoRestoreWinView()
endif

" [Sync syntax highlighting]
" https://stackoverflow.com/a/17189261/963198
noremap <F12> <Esc>:syntax sync fromstart<CR>
inoremap <F12> <C-o>:syntax sync fromstart<CR>

" To make Meta/Alt mappings work, use this work around
" [Alt key shortcuts not working on gnome terminal with Vim]
" https://stackoverflow.com/a/10216459
let c='a'
while c <= 'z'
  exec "set <M-".c.">=\e".c
  exec "imap \e".c." <M-".c.">"
  let c = nr2char(1+char2nr(c))
endw
set ttimeout ttimeoutlen=2

